# Cake List - ObjC fix and Swift conversion
## The `Readme.md`

Hi. 
For my submission for this technical challenge, I've opted to recreate the app from scratch in Swift.
I have also included the fixed Objective-C version too as that was the actual challenge, but figured I'd start from scratch on the Swift side too, purely because the app is so trivial and, to be honest, for the fun of it.

So, let's break this down.

>We would like you to fix the crash bug, 

This crash was caused by the cell identifier being incorrect in the `cellForRowAtIndexPath:` function. Correcting this to 'CakeCell' remedies that crash at launch.

---------
>ensure the functionality of the app works as expected (all images display correctly on the table, all text is readable) 

There were some glaring errors with the UI these were:

**Massive cell images**
`UIImageView` in the custom cell having its content mode to 'center' rather than something like 'Aspect fit' or 'Aspect fill'.

**Truncated and off-centre labels**
The two labels in the cell are a small, fixed size, meaning that all the labels are truncated. This can be fixed by either making them a larger fixed size or, preferably, use auto-layouts.

**Images not loading are due to app transport security.**
Some of the images loaded are 'http', meaning they're getting blocked by ATS. Some of the images should, in actual fact, be using https (bbc links, for example). To resolve this, I have put in a kind of sanitisation that attempts to change the url to https before trying to fetch those images. The idea is that we can potentially get the url working, but if the link is actually insecure, the app will gracefully fail it and set a placeholder 'unable to load' image. The, no-good (for prod) solution, would be to disable ATS.

--------
>performs smoothly (ideally, this should be done without third party libraries). 

The obvious issue with this application is that is loads image data, synchronously on the main thread. This causes the UI thread to lock, making the application unresponsive until all image data is loaded. Furthermore, the app does not cache the image data, so as the table is scrolled, the image data is repeatedly loaded, causing further thread locks.
This is resolved by creating a service layer that loads image data asynchronously, which utilises a URLCache to speed up the retrieval of any reused images, as well as any successive launches of the app. The cache size is set to a small 25MB as to not bloat the app (if this were to scale in to a full application).

-------

>You should also refactor, optimise and improve the code where appropriate to use platform best practises.

For this, I have refactored the code to use a more structured design pattern. As, I'm sure I'll mention further down, I didn't go over the top with this as to not spend all day on it, but rather to make it more than adequate for the task at hand. I have gone for a simple MVC pattern, but quite suitably have gone full-hog using MVVM and dependency injection to make the application thoroughly testable.

------
>but shouldn’t be over engineered. 

Hence not going over the top with creating a dependency injection framework for the service layer

------
>The test can be completed in 2-3 hours; howerever, this is a not a limit. Feel free to spend more time on it if you wish.

I did wish. I probably ended up on the side of 3-4 hours in total, I reckon, less this document.

