//
//  Cake.h
//  Cake List
//
//  Created by Luke Sadler on 17/08/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol CakeDelegate <NSObject>

-(void)cakeImageUpdated;
@end

@interface Cake : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) UIImage *image;

@property (weak) id <CakeDelegate> delegate;

-(instancetype)initWithDict:(NSDictionary *)dict;

@end
