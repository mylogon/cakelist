//
//  Cake.m
//  Cake List
//
//  Created by Luke Sadler on 17/08/2018.
//  Copyright � 2018 Stewart Hart. All rights reserved.
//

#import "Cake.h"

@implementation Cake

-(instancetype)initWithDict:(NSDictionary *)dict{
    if (self = [super init]) {
        
        _title = dict[@"title"];
        _desc = dict[@"desc"];
        _imageUrl = dict[@"image"];
        
        if (!_title || !_desc || !_imageUrl) {
            NSLog(@"There's something wrong with this object deserialisation");
        }
        
    }
    return self;
}

-(void)setImage:(UIImage *)image{
    self->_image = image;
    
    if ([_delegate respondsToSelector:@selector(cakeImageUpdated)]) {
        [_delegate cakeImageUpdated];    
    } else {
        NSLog(@"Delegate has not implemented 'cakeImageUpdated'");
    }
}

@end
