//
//  HttpService.h
//  Cake List
//
//  Created by Luke Sadler on 17/08/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface HttpService : NSObject

-(void)getMeMyCakeList:(void (^)(NSArray <NSDictionary *> * cakes, NSError * error))complete;

-(void)getCakeImage:(NSString *)urlString complete:(void(^)(UIImage *))complete;

@end
