//
//  HttpService.m
//  Cake List
//
//  Created by Luke Sadler on 17/08/2018.
//  Copyright © 2018 Stewart Hart. All rights reserved.
//

#import "HttpService.h"

NSString * const cakeListURLString = @"https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

@interface HttpService ()

@property UIImage *errorImage;
@property NSURLCache *cache;

@end

@implementation HttpService


-(instancetype)init{
    if (self = [super init]) {

        int cacheSizeMB = 1024 * 1024 * 25;
        
        _cache = [[NSURLCache alloc]initWithMemoryCapacity:cacheSizeMB
                                              diskCapacity:cacheSizeMB
                                                  diskPath:@"cachePath"];
        _errorImage = [UIImage imageNamed:@"errorLoading"];
    }
    return self;
}


-(void)getMeMyCakeList:(void (^)(NSArray <NSDictionary *> *, NSError *))complete{

    NSURL *listURL = [NSURL URLWithString:cakeListURLString];
    
    if (!listURL) {
        NSLog(@"url is invalid");
        return;
    }
    
     NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:listURL
                                 completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                  
                                     if (!error && data) {
                                         
                                         NSError *err;
                                         
                                         NSArray *array = [NSJSONSerialization JSONObjectWithData:data
                                                                                          options:0
                                                                                            error:&err];
                                         
                                         if (err) {
                                             NSLog(@"%@",err.localizedDescription);
                                             complete(nil,err);
                                             return;
                                         }
                                         
                                         
                                         complete(array,nil);
                                     }else{
                                         
                                         NSLog(@"error with list load");
                                         complete(nil, error);
                                     }
                                     
                                 }];
    
    [task resume];
}

-(void)getCakeImage:(NSString *)urlString
           complete:(void (^)(UIImage *))complete{
    
    void (^RespondImage)(UIImage *) = ^(UIImage * image) {
        if (image) {
            complete(image);
        }else{
            complete(_errorImage);
        }
    };
    
    
    NSString *sanitisedUrlString = [self sanitiseUrlString:urlString];
    NSURL *imageUrl = [NSURL URLWithString:sanitisedUrlString];
    
    if (!imageUrl) {
        RespondImage(nil);
        return;
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:imageUrl];
    
    NSCachedURLResponse * cachedResponse = [_cache cachedResponseForRequest:request];
    
    if (cachedResponse.data) {
        UIImage *image = [UIImage imageWithData:cachedResponse.data];
        RespondImage(image);
        
        NSLog(@"image was retrieved from cache");
    }else{
    
        NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
        config.URLCache = _cache;
        
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
        
        NSURLSessionTask * task = [session dataTaskWithRequest:request
                                             completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                 
                                                 if (data && !error) {
                                                     
                                                     UIImage *cakeImage = [UIImage imageWithData:data];
                                                     RespondImage(cakeImage);
                                                     
                                                     NSLog(@"retrieved image from interwebs");
                                                 }else{
                                                     RespondImage(nil);
                                                 }
                                             }];
        
        [task resume];
    }
    
    
}

-(NSString *)sanitiseUrlString:(NSString *)urlString {
    return [urlString stringByReplacingOccurrencesOfString:@"http://" withString:@"https://"];
}

@end
