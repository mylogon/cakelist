//
//  MasterViewController.m
//  Cake List
//
//  Created by Stewart Hart on 19/05/2015.
//  Copyright (c) 2015 Stewart Hart. All rights reserved.
//

#import "MasterViewController.h"
#import "CakeCell.h"
#import "HttpService.h"
#import "Cake.h"

@interface MasterViewController () <CakeDelegate>
@property (strong, nonatomic) NSArray <Cake *> *objects;
@property (strong, nonatomic) HttpService *httpService;
@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _httpService = HttpService.new;
    
    [_httpService getMeMyCakeList:^(NSArray <NSDictionary *> *cakeDicts, NSError *error) {
        
        NSMutableArray * cakesArray = NSMutableArray.new;
        
        for (NSDictionary *dict in cakeDicts) {
            Cake *cake = [[Cake alloc]initWithDict:dict];
            cake.delegate = self;
            cake.image = [UIImage imageNamed:@"placeholder"];
            
            [_httpService getCakeImage:cake.imageUrl
                              complete:^(UIImage * image) {
                                  cake.image = image;
                              }];
            [cakesArray addObject:cake];
        }
        
        self.objects = cakesArray;
        
    }];
    
}

-(void)cakeImageUpdated{
    [NSOperationQueue.mainQueue addOperationWithBlock:^{
        [self.tableView reloadData];
    }];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CakeCell *cell = (CakeCell*)[tableView dequeueReusableCellWithIdentifier:@"CakeCell"];
    
    Cake *cake = self.objects[indexPath.row];
    
    cell.titleLabel.text = cake.title;
    cell.descriptionLabel.text = cake.desc;
    cell.cakeImageView.image = cake.image;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
