//
//  CakeTableViewCell.swift
//  CakeList
//
//  Created by Luke Sadler on 17/08/2018.
//  Copyright © 2018 luke. All rights reserved.
//

import UIKit

class CakeTableViewCell: UITableViewCell {

    @IBOutlet weak var cakeImageView: UIImageView!
    @IBOutlet weak var cakeTitleLabel: UILabel!
    @IBOutlet weak var cakeSubtitleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cakeImageView.contentMode = .scaleAspectFill
        self.cakeImageView.layer.cornerRadius = 2
        
        self.cakeImageView.layer.masksToBounds = true
    }
    
}
