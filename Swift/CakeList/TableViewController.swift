//
//  TableViewController.swift
//  CakeList
//
//  Created by Luke Sadler on 16/08/2018.
//  Copyright © 2018 luke. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, CakeDelegate {

    let httpService = HttpService()
    var tableData: [Cake]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Cake List"
        
        httpService.getMeMyCakeList { (cakeDicts, error) in
            
            if let cakeDicts = cakeDicts {
                
                let cakes = cakeDicts.compactMap(Cake.init)
                
                self.tableData = cakes
                self.tableView.reloadData()
                
                cakes.forEach({ (cake) in
                    
                    cake.image = UIImage.init(named: "placeholder")
                    cake.delegate = self
                    
                    self.httpService.getCakeImage(cakeImageAddress: cake.imageUrlString,
                                                  complete: { (image) in
                                                    cake.image = image
                    })
                })
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func cakeImageWasUpdated() {
        DispatchQueue.main.async {
            self.tableView.reloadData()            
        }
    }
    
    //Table delegates
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> CakeTableViewCell {
        
        let cellID = "cellIdentifier"
        
        let getCell: () -> CakeTableViewCell? = {
            return tableView.dequeueReusableCell(withIdentifier: cellID) as? CakeTableViewCell
        }

        let cake = tableData?[indexPath.row]
        
        var cell = getCell()
        
        if cell == nil {
            let cakeCellNib = UINib.init(nibName: "CakeTableViewCell", bundle: Bundle.main)
            tableView.register(cakeCellNib, forCellReuseIdentifier: cellID)
            cell = getCell()
        }
        
        cell?.cakeTitleLabel.text = cake?.title
        cell?.cakeSubtitleLabel.text = cake?.desc
        cell?.cakeImageView.image = cake?.image
        
        return cell ?? CakeTableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}

