//
//  Cake.swift
//  CakeList
//
//  Created by Luke Sadler on 16/08/2018.
//  Copyright © 2018 luke. All rights reserved.
//

import UIKit

protocol CakeDelegate {
    func cakeImageWasUpdated()
}

class Cake: NSObject {

    var delegate: CakeDelegate?
    
    var title: String?
    var desc: String?
    var imageUrlString: String?
    
    var image: UIImage? {
        didSet{
            delegate?.cakeImageWasUpdated()
        }
    }
    
    init?(dict: Dictionary <String, Any>){
        
        guard let title = dict["title"] as? String,
            let desc = dict["desc"] as? String,
            let imageUrlString = dict["image"] as? String else {
                
            return nil
        }
        
        self.title = title
        self.desc = desc
        self.imageUrlString = imageUrlString
    }
    
}
