//
//  HttpService.swift
//  CakeList
//
//  Created by Luke Sadler on 16/08/2018.
//  Copyright © 2018 luke. All rights reserved.
//

import UIKit

class HttpService: NSObject {

    let cakeListUrlString = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json"
    
    let errorImage = UIImage.init(named: "errorLoading")
    
    static let cacheSizeMB = 1024 * 1024 * 25
    let cache = URLCache.init(memoryCapacity: cacheSizeMB,
                              diskCapacity: cacheSizeMB,
                              diskPath: "cachePath")
    
    
    func getMeMyCakeList (complete: @escaping ([Dictionary<String, Any>]?, Error?) -> ()){
        
        guard let cakeUrl = URL.init(string: cakeListUrlString) else {
            print("your url is duff. handle your mistakes here")
            return
        }
        
        URLSession.shared.dataTask(with: cakeUrl) { (data, _, err) in
            
            if let data = data {
                
                do {
                    
                    let array = try JSONSerialization.jsonObject(with: data,
                                                             options: .mutableContainers) as? [Dictionary<String, Any>]
                    
                    if let array = array {
                        
                        DispatchQueue.main.async {
                            complete(array,nil)
                        }
                        
                    }else{
                     
                        print("Not quite right, matey")
                    }
                    
                } catch {
                    
                    complete(nil, error)
                }
                
            }else{
                
                complete(nil, err)
            }
            
        }.resume()
    }
    
    
    func getCakeImage (cakeImageAddress: String?, complete: @escaping (UIImage?) -> ()) {
        
        let setImage: (UIImage?) -> () = {(image) in
         
            if let image = image {
                complete(image)
            }else{
                complete(self.errorImage)
            }
        }
        
        guard let sanitisedString = sanitiseUrl(urlString: cakeImageAddress) else {
            setImage(nil)
            return
        }
        
        guard let imageUrl = URL.init(string: sanitisedString) else {
            setImage(nil)
            return
        }

        let request = URLRequest.init(url: imageUrl)
        
        if let cachedResponseData = cache.cachedResponse(for: request)?.data {
            let cachedImage = UIImage.init(data: cachedResponseData)
            setImage(cachedImage)
        }else{
        
            let config = URLSessionConfiguration.default
            config.urlCache = cache
            
            let urlSession = URLSession.init(configuration: config)
            urlSession.dataTask(with: request) { (imageData, _, error) in
                
                if let imageData = imageData {
                    
                    if let image = UIImage.init(data: imageData) {
                        setImage(image)
                    }else{
                        setImage(nil)
                    }
                }else{
                    setImage(nil)
                }
                
            }.resume()
            
        }
        
    }
    
    
    // Here we're giving those poor image urls a chance to redeem themselves.
    // Some appear to be falsly accused of being http and I'm not turning ATS off in a hurry.
    // If they're truely not https, they'll fail anyway
    func sanitiseUrl (urlString: String?) -> String? {
        return urlString?.replacingOccurrences(of: "http://", with: "https://")
    }
    
}
